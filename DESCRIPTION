Package: rPBK
Title: Inference and Prediction of Generic Physiologically-Based Kinetic Models
Version: 0.2.3
Authors@R: c(
    person("Virgile", "Baudrot", role = c("aut","cre"), email = "virgile.baudrot@qonfluens.com"),
    person("Sandrine", "Charles", role = "aut"),
    person("Christelle", "Lopes", role = "aut"),
    person("Ophélia", "Gestin", role = "ctb"),
    person("Dominique", "Lamonica", role = "ctb"),
    person("Aurélie", "Siberchicot", role = "ctb")
    )
BugReports: https://gitlab.com/qonfluens/model/rPBK
URL: https://gitlab.com/qonfluens/model/rPBK
Description: Fit and simulate any kind of
    physiologically-based kinetic ('PBK') models whatever the number of compartments.
    Moreover, it allows to account for any link between pairs of compartments, as
    well as any link of each of the compartments with the external medium. Such
    generic PBK models have today applications in pharmacology (PBPK models) to
    describe drug effects, in toxicology and ecotoxicology (PBTK models) to describe
    chemical substance effects. In case of exposure to a parent compound (drug or
    chemical) the 'rPBK' package allows to consider metabolites, whatever their number
    and their phase (I, II, ...). Last but not least, package 'rPBK' can also be used for
    dynamic flux balance analysis (dFBA) to deal with metabolic networks. See also
    Charles et al. (2022) <doi:10.1101/2022.04.29.490045>.
License: MIT + file LICENSE
Encoding: UTF-8
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.3.1
Biarch: true
Depends: 
    R (>= 3.4.0)
Imports: 
    ggplot2,
    methods,
    Rcpp (>= 0.12.0),
    rstan (>= 2.26.0),
    rstantools
LinkingTo: 
    BH (>= 1.66.0),
    Rcpp (>= 0.12.0),
    RcppEigen (>= 0.3.3.3.0),
    RcppParallel (>= 5.0.1),
    rstan (>= 2.26.0),
    StanHeaders (>= 2.26.0)
SystemRequirements: GNU make
Suggests: 
    knitr,
    loo,
    rmarkdown,
    testthat
VignetteBuilder: knitr
Config/testthat/edition: 3
LazyData: true
